class Home < ApplicationRecord
  belongs_to :model

  has_many :widget_home
  has_many :widgets, :through => :widget_home

end

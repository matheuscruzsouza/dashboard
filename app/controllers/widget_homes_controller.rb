class WidgetHomesController < ApplicationController
  before_action :set_widget_home, only: [:show, :edit, :update, :destroy]

  # GET /widget_homes
  # GET /widget_homes.json
  def index
    @widget_homes = WidgetHome.all
  end

  # GET /widget_homes/1
  # GET /widget_homes/1.json
  def show
  end

  # GET /widget_homes/new
  def new
    @widget_home = WidgetHome.new
  end

  # GET /widget_homes/1/edit
  def edit
  end

  # POST /widget_homes
  # POST /widget_homes.json
  def create
    @widget_home = WidgetHome.new(widget_home_params)

    respond_to do |format|
      if @widget_home.save
        format.html { redirect_to @widget_home, notice: 'Widget home was successfully created.' }
        format.json { render :show, status: :created, location: @widget_home }
      else
        format.html { render :new }
        format.json { render json: @widget_home.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /widget_homes/1
  # PATCH/PUT /widget_homes/1.json
  def update
    respond_to do |format|
      if @widget_home.update(widget_home_params)
        format.html { redirect_to @widget_home, notice: 'Widget home was successfully updated.' }
        format.json { render :show, status: :ok, location: @widget_home }
      else
        format.html { render :edit }
        format.json { render json: @widget_home.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /widget_homes/1
  # DELETE /widget_homes/1.json
  def destroy
    @widget_home.destroy
    respond_to do |format|
      format.html { redirect_to request.referrer, notice: 'Widget home was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_widget_home
      @widget_home = WidgetHome.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def widget_home_params
      params.require(:widget_home).permit(:width, :height, :background_color, :widget_id, :home_id)
    end
end

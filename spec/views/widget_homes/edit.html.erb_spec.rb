require 'rails_helper'

RSpec.describe "widget_homes/edit", type: :view do
  before(:each) do
    @widget_home = assign(:widget_home, WidgetHome.create!(
      :width => 1,
      :height => 1,
      :widget => nil,
      :home => nil
    ))
  end

  it "renders the edit widget_home form" do
    render

    assert_select "form[action=?][method=?]", widget_home_path(@widget_home), "post" do

      assert_select "input[name=?]", "widget_home[width]"

      assert_select "input[name=?]", "widget_home[height]"

      assert_select "input[name=?]", "widget_home[widget_id]"

      assert_select "input[name=?]", "widget_home[home_id]"
    end
  end
end

require 'rails_helper'

RSpec.describe "widgets/index", type: :view do
  before(:each) do
    assign(:widgets, [
      Widget.create!(
        :nome => "Nome"
      ),
      Widget.create!(
        :nome => "Nome"
      )
    ])
  end

  it "renders a list of widgets" do
    render
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
  end
end

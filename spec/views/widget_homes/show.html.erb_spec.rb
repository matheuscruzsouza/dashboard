require 'rails_helper'

RSpec.describe "widget_homes/show", type: :view do
  before(:each) do
    @widget_home = assign(:widget_home, WidgetHome.create!(
      :width => 2,
      :height => 3,
      :widget => nil,
      :home => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end

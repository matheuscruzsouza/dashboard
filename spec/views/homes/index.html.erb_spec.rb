require 'rails_helper'

RSpec.describe "homes/index", type: :view do
  before(:each) do
    assign(:homes, [
      Home.create!(
        :model => nil,
        :nome => "Nome"
      ),
      Home.create!(
        :model => nil,
        :nome => "Nome"
      )
    ])
  end

  it "renders a list of homes" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
  end
end

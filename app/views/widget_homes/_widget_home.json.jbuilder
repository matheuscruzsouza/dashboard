json.extract! widget_home, :id, :width, :height, :widget_id, :home_id, :created_at, :updated_at
json.url widget_home_url(widget_home, format: :json)

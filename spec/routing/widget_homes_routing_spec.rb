require "rails_helper"

RSpec.describe WidgetHomesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/widget_homes").to route_to("widget_homes#index")
    end

    it "routes to #new" do
      expect(:get => "/widget_homes/new").to route_to("widget_homes#new")
    end

    it "routes to #show" do
      expect(:get => "/widget_homes/1").to route_to("widget_homes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/widget_homes/1/edit").to route_to("widget_homes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/widget_homes").to route_to("widget_homes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/widget_homes/1").to route_to("widget_homes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/widget_homes/1").to route_to("widget_homes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/widget_homes/1").to route_to("widget_homes#destroy", :id => "1")
    end

  end
end

class AddColorToWidgetHome < ActiveRecord::Migration[5.1]
  def change
    add_column :widget_homes, :background_color, :string, default: "white"
  end
end

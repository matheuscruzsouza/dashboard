require 'rails_helper'

RSpec.describe "widget_homes/new", type: :view do
  before(:each) do
    assign(:widget_home, WidgetHome.new(
      :width => 1,
      :height => 1,
      :widget => nil,
      :home => nil
    ))
  end

  it "renders new widget_home form" do
    render

    assert_select "form[action=?][method=?]", widget_homes_path, "post" do

      assert_select "input[name=?]", "widget_home[width]"

      assert_select "input[name=?]", "widget_home[height]"

      assert_select "input[name=?]", "widget_home[widget_id]"

      assert_select "input[name=?]", "widget_home[home_id]"
    end
  end
end

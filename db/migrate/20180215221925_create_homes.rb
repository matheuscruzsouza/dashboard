class CreateHomes < ActiveRecord::Migration[5.1]
  def change
    create_table :homes do |t|
      t.references :model, foreign_key: true
      t.string :nome

      t.timestamps
    end
  end
end

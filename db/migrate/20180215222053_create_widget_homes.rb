class CreateWidgetHomes < ActiveRecord::Migration[5.1]
  def change
    create_table :widget_homes do |t|
      t.integer :width
      t.integer :height
      t.references :widget, foreign_key: true
      t.references :home, foreign_key: true

      t.timestamps
    end
  end
end

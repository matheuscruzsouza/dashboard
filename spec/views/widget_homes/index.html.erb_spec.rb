require 'rails_helper'

RSpec.describe "widget_homes/index", type: :view do
  before(:each) do
    assign(:widget_homes, [
      WidgetHome.create!(
        :width => 2,
        :height => 3,
        :widget => nil,
        :home => nil
      ),
      WidgetHome.create!(
        :width => 2,
        :height => 3,
        :widget => nil,
        :home => nil
      )
    ])
  end

  it "renders a list of widget_homes" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end

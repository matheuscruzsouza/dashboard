require 'rails_helper'

RSpec.describe "homes/new", type: :view do
  before(:each) do
    assign(:home, Home.new(
      :model => nil,
      :nome => "MyString"
    ))
  end

  it "renders new home form" do
    render

    assert_select "form[action=?][method=?]", homes_path, "post" do

      assert_select "input[name=?]", "home[model_id]"

      assert_select "input[name=?]", "home[nome]"
    end
  end
end
